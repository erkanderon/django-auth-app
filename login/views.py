
from django.views import generic
#from .models import Album
#from django.views.generic.edit import CreateView, UpdateView, DeleteView
#from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.views.generic import View
#from .models import Album
from .forms import UserForm, UserLoginForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import logout
from django.views.generic import RedirectView
from django.urls import reverse


from task.models import *
from task.forms import UserAddTaskListForm


class LoginView(generic.ListView):
	model = TaskList
	template_name = "login/login.html"

	def get_queryset(self):
		return None

class HomeView(LoginRequiredMixin, generic.ListView):
	template_name = "login/home.html"
	context_object_name = 'all_tasks_by_user'
	form_class = UserAddTaskListForm

	def get(self, request):
		form = self.form_class(None)
		return render(request, self.template_name, {'form': form})

class UserFormView(View):
	form_class = UserForm
	template_name = "login/registration_form.html"


	def get(self, request):
		form = self.form_class(None)
		return render(request, self.template_name, {'form': form})

	def post(self, request):
		form = self.form_class(request.POST)

		if form.is_valid():

			user = form.save(commit = False)

			# cleaned normalized data

			username = form.cleaned_data['username']
			password = form.cleaned_data['password']

			user.set_password(password)
			user.save()

			# returns User object if the credentials are true

			user = authenticate(username=username, password = password)

			if user is not None:
				if user.is_active:
					login(request, user)
					return redirect('/')

		return render(request, self.template_name, {'form': form})

class UserLoginView(View):
	form_class = UserLoginForm
	form_class_reg = UserForm
	template_name = "login/registration_form.html"



	# display blank form
	def get(self, request):
		form = self.form_class(None)

		return render(request, self.template_name, {'form': UserForm})

	def post(self, request):
		form  = self.form_class(request.POST)

		if form.is_valid():

			username = form.cleaned_data['username']
			password = form.cleaned_data['password']

			user = authenticate(username=username, password = password)
			
			if user is not None:
				if user.is_active:
					login(request, user)
					return redirect('/home/')

		return render(request, self.template_name, {'form': UserForm})

class LogoutView(View):

	template_name = "login/logout.html"


	# display blank form
	def get(self, request):
		if self.request.user.is_authenticated:
			logout(self.request)
		return render(request, self.template_name)




