from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from .models import *
from django.urls import reverse_lazy
from pymeet.utils import *
from django.http import HttpResponse
from .forms import *
import datetime
from django.http import HttpResponseRedirect


class TasksView(LoginRequiredMixin, generic.ListView):
	template_name = "login/home.html"
	#context_object_name = 'all_albums'

	def get_queryset(self):
		return None


class TasksDetailView(LoginRequiredMixin, generic.ListView):
	model = Task
	template_name = "tasks/task_detail.html"
	context_object_name = 'task_details'
	form_class = UserAddTaskForm

	#def get_queryset(self, **kwargs):
	#	return Task.objects.filter(task_owner=self.kwargs["pk"])

	def get(self, request, *args, **kwargs):

		return render(request, 'tasks/task_detail.html', 
			{'task_details': Task.objects.filter(task_owner=self.kwargs["pk"]), 'add_form': UserAddTaskForm})
		#return Task.objects.filter(task_owner=self.kwargs["pk"])

	def post(self, request, *args, **kwargs):

		is_done = request.POST.get('is_done')
		tk = request.POST.get('pk')

		try:
			obj = Task.objects.get(pk=tk)
			obj.is_done = check_is_true(is_done)
			obj.save()
		except:
			return HttpResponse(status=500)
		
		return render(request, self.template_name, 
			{'task_details': Task.objects.filter(task_owner=self.kwargs["pk"]), 'add_form': UserAddTaskForm})




class TaskDelete(LoginRequiredMixin, generic.DeleteView):
	model = Task

	def get_success_url(self):
		return reverse_lazy('task:detail_task', kwargs={'pk': self.kwargs["tk"]})

class AddTask(LoginRequiredMixin, generic.ListView):
	model = Task
	form_class = UserAddTaskForm
	template_name = "tasks/task_detail.html"

	def get(self, request):
		return None

	def post(self, request, *args, **kwargs):

		form = self.form_class(request.POST)

		if form.is_valid():

			task = form.save(commit = False)
			next = request.POST.get('next', '/')

			# cleaned normalized data (Date field ex)

			task.task_name = form.cleaned_data['task_name']
			task.task_owner = TaskList.objects.get(pk=self.kwargs["pk"])
			task.desc = ""
			task.save()

			
			return HttpResponseRedirect(next)

class AddTaskList(LoginRequiredMixin, generic.ListView):
	model = TaskList
	form_class = UserAddTaskListForm

	def post(self, request, *args, **kwargs):

		form = self.form_class(request.POST)

		if form.is_valid():

			tasklist = form.save(commit = False)
			tasklist.owner = request.user
			tasklist.save()

			
			return HttpResponseRedirect("/home")


