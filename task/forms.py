from django import forms
from .models import *
from django.contrib.auth.models import User


class UserAddTaskForm(forms.ModelForm):

	task_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Add Todo'}))

	class Meta:
		model = Task
		fields = ['task_name']


class UserAddTaskListForm(forms.ModelForm):

	'''users = User.objects.all()
	CHOICES = ()

	if users:
		for user in users:
			CHOICES = CHOICES + ((user, user.username),)

	owner = forms.ChoiceField(choices=CHOICES)'''

	class Meta:
		model = TaskList
		fields = ['list_title']