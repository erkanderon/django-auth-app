
from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User



class TaskList(models.Model):
	owner = models.ForeignKey(User, on_delete=models.CASCADE)
	list_title = models.CharField(max_length=100)

	def get_absolute_url(self):
		return reverse('task:detail', kwargs={'pk':self.pk})

	def __str__(self):
		return self.list_title + ' - ' + str(self.owner)


class Task(models.Model):
	task_owner = models.ForeignKey(TaskList, on_delete=models.CASCADE)
	task_name = models.CharField(max_length=250)
	task_desc = models.CharField(max_length=250)
	task_deadline = models.DateTimeField(auto_now_add=True)
	is_done = models.BooleanField(default = False)

	def __str__(self):
		return str(self.task_name)




