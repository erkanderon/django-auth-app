from django.conf.urls import url
from . import views

app_name = "task"

urlpatterns = [

    ##url(r'^$', views.LoginView.as_view(), name='login'),
    url(r'^$', views.TasksView.as_view(), name='tasks'),
    url(r'^(?P<pk>[0-9]+)/$', views.TasksDetailView.as_view(), name='detail_task'),
    url(r'^(?P<tk>[0-9]+)/delete/(?P<pk>[0-9]+)/$', views.TaskDelete.as_view(), name='delete_task'),
    url(r'^(?P<pk>[0-9]+)/add/$', views.AddTask.as_view(), name='add_task'),
    url(r'^add-task-list/$', views.AddTaskList.as_view(), name='add_task_list'),
    
    
    
]