from task.models import TaskList


def add_variable_to_context(request):
	if request.user.is_authenticated:
		return {'task_list': TaskList.objects.filter(owner=request.user)}
	else:
		return {}